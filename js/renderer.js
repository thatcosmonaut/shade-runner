// this class gettin too fat

const ShaderCompiler = require('./shader-compiler.js')
const DefaultShader = require('./generators/default.js')
const StandardShaderGenerator = require('./generators/standard.js')
const TextureUtils = require('./texture-utils.js')

module.exports = class {
  constructor (container, width, height, errorDisplayer) {
    this.width = width
    this.height = height
    this.container = container

    this.canvas = document.createElement('canvas')
    this.container.appendChild(this.canvas)

    this.surface = { centerX: 0, centerY: 0, width: 1, height: 1, isPanning: false, isZooming: false, lastX: 0, lastY: 0 }

    this.gl = this.canvas.getContext('webgl2', { preserveDrawingBuffer: true })
    this.gl.getExtension('OES_standard_derivatives')
    this.buffer = this.gl.createBuffer()
    this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.buffer)
    this.gl.bufferData(this.gl.ARRAY_BUFFER, new Float32Array([-1.0, -1.0, 1.0, -1.0, -1.0, 1.0, 1.0, -1.0, 1.0, 1.0, -1.0, 1.0]), this.gl.STATIC_DRAW)

    this.surface.buffer = this.gl.createBuffer()

    this.vertexPosition = null
    this.shaderProgram = null

    this.resize(this.width, this.height)

    this.date = new Date()

    this.mouseClickPosition = {
      x: 0,
      y: 0
    }

    this.channels = [null, null, null, null]
    this.channelResolutions = [
      {w: 0, h: 0},
      {w: 0, h: 0},
      {w: 0, h: 0},
      {w: 0, h: 0}
    ]

    TextureUtils.loadTextureFromURL(this.gl, 'images/blank.png', (texture, width, height) => {
      this.setChannelTexture(0, texture, width, height)
      this.setChannelTexture(1, texture, width, height)
      this.setChannelTexture(2, texture, width, height)
      this.setChannelTexture(3, texture, width, height)
    })

    this.startTime = Date.now()

    this.errorDisplayer = errorDisplayer
    this.shaderCompiler = new ShaderCompiler(this.gl, this.errorDisplayer)

    this.newShaderCode(DefaultShader.fragmentShader)
    this.newScreenCode()
  }

  setChannelTexture (channelNum, texture, width, height) {
    this.channels[channelNum] = texture
    this.channelResolutions[channelNum].w = width
    this.channelResolutions[channelNum].h = height
  }

  resize (width, height) {
    this.width = width
    this.height = height

    this.container.width = this.width
    this.container.height = this.height

    this.canvas.width = this.width
    this.canvas.height = this.height

    this._computeSurfaceCorners()

    this.gl.viewport(0, 0, this.width, this.height)
    this._createRenderTargets()
  }

  _computeSurfaceCorners () {
    this.surface.width = this.width
    this.surface.height = this.height

    let halfWidth = this.surface.width * 0.5
    let halfHeight = this.surface.height * 0.5

    this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.surface.buffer)
    this.gl.bufferData(this.gl.ARRAY_BUFFER, new Float32Array([
      this.surface.centerX - halfWidth, this.surface.centerY - halfHeight,
      this.surface.centerX + halfWidth, this.surface.centerY - halfHeight,
      this.surface.centerX - halfWidth, this.surface.centerY + halfHeight,
      this.surface.centerX + halfWidth, this.surface.centerY - halfHeight,
      this.surface.centerX + halfWidth, this.surface.centerY + halfHeight,
      this.surface.centerX - halfWidth, this.surface.centerY + halfHeight]), this.gl.STATIC_DRAW)
  }

  _createRenderTargets () {
    this.frontTarget = this._createTarget(this.width, this.height)
    this.backTarget = this._createTarget(this.width, this.height)
  }

  _createTarget (width, height) {
    let target = {}

    target.framebuffer = this.gl.createFramebuffer()
    target.renderbuffer = this.gl.createRenderbuffer()
    target.texture = this.gl.createTexture()

    this.gl.bindTexture(this.gl.TEXTURE_2D, target.texture)
    this.gl.texImage2D(this.gl.TEXTURE_2D, 0, this.gl.RGBA, width, height, 0, this.gl.RGBA, this.gl.UNSIGNED_BYTE, null)
    this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_S, this.gl.CLAMP_TO_EDGE)
    this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_T, this.gl.CLAMP_TO_EDGE)
    this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MAG_FILTER, this.gl.NEAREST)
    this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MIN_FILTER, this.gl.NEAREST)

    this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, target.framebuffer)
    this.gl.framebufferTexture2D(this.gl.FRAMEBUFFER, this.gl.COLOR_ATTACHMENT0, this.gl.TEXTURE_2D, target.texture, 0)

    this.gl.bindRenderbuffer(this.gl.RENDERBUFFER, target.renderbuffer)
    this.gl.renderbufferStorage(this.gl.RENDERBUFFER, this.gl.DEPTH_COMPONENT16, width, height)
    this.gl.framebufferRenderbuffer(this.gl.FRAMEBUFFER, this.gl.DEPTH_ATTACHMENT, this.gl.RENDERBUFFER, target.renderbuffer)

    this.gl.bindTexture(this.gl.TEXTURE_2D, null)
    this.gl.bindRenderbuffer(this.gl.RENDERBUFFER, null)
    this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, null)
    return target
  }

  _compileShader (vertexCode, fragmentCode) {
    this.errorDisplayer.clearErrors()

    let vs = this.shaderCompiler.compileVertexShader(vertexCode)
    let fs = this.shaderCompiler.compileFragmentShader(fragmentCode)

    if (vs == null || fs == null) return null

    let program = this.shaderCompiler.link(vs, fs)

    if (program == null) return null

    return program
  }

  _compileScreenProgram () {
    let fragmentShader = `precision mediump float;
    uniform vec2 resolution;
    uniform sampler2D texture;
    void main() {
      vec2 uv = gl_FragCoord.xy / resolution.xy;
      gl_FragColor = texture2D( texture, uv );
    }`

    let vertexShader = `
      attribute vec3 position;
      void main() {
        gl_Position = vec4( position, 1.0 );
      }`

    let vs = this.shaderCompiler.compileVertexShader(vertexShader)
    let fs = this.shaderCompiler.compileFragmentShader(fragmentShader)

    if (vs == null || fs == null) return null

    let program = this.shaderCompiler.link(vs, fs)

    if (program == null) return null

    return program
  }

  _cacheUniformLocation (program, label) {
    if (program.uniformsCache === undefined) {
      program.uniformsCache = {}
    }

    program.uniformsCache[label] = this.gl.getUniformLocation(program, label)
  }

  newScreenCode () {
    let program = this._compileScreenProgram()
    if (program == null) return null

    this._cacheUniformLocation(program, 'resolution')
    this._cacheUniformLocation(program, 'texture')
    this.screenVertexPosition = this.gl.getAttribLocation(program, 'position')
    this.gl.enableVertexAttribArray(this.screenVertexPosition)

    this.screenProgram = program
  }

  newShaderCode (shaderCode) {
    let vertexCode = DefaultShader.surfaceVertexShader
    let fragmentCode = StandardShaderGenerator.fragShader(shaderCode)

    let program = this._compileShader(vertexCode, fragmentCode)
    if (program == null) return null

    this._cacheUniformLocation(program, 'resolution')
    this._cacheUniformLocation(program, 'time')
    this._cacheUniformLocation(program, 'frame')
    this._cacheUniformLocation(program, 'frameRate')
    this._cacheUniformLocation(program, 'mouse')
    this._cacheUniformLocation(program, 'date')
    this._cacheUniformLocation(program, 'offset')
    this._cacheUniformLocation(program, 'channel0')
    this._cacheUniformLocation(program, 'channel1')
    this._cacheUniformLocation(program, 'channel2')
    this._cacheUniformLocation(program, 'channel3')
    this._cacheUniformLocation(program, 'channelResolution')

    this._cacheUniformLocation(program, 'backbuffer')
    this._cacheUniformLocation(program, 'surfaceSize')

    this.currentProgram = program

    this.gl.useProgram(this.currentProgram)

    this.surface.positionAttribute = this.gl.getAttribLocation(this.currentProgram, 'surfacePosAttrib')
    this.gl.enableVertexAttribArray(this.surface.positionAttribute)
    this.vertexPosition = this.gl.getAttribLocation(this.currentProgram, 'position')
    this.gl.enableVertexAttribArray(this.vertexPosition)

    this.resetTime()
  }

  mouseClick (x, y) {
    this.mouseClickPosition.x = x
    this.mouseClickPosition.y = y
  }

  resetTime () {
    this.startTime = Date.now()
    this.frameCount = 0
  }

  update () {
    let thisUpdate = window.performance.now()

    this.frameRate = 1000 / (thisUpdate - this.lastUpdate)
    this.elapsedTime = (Date.now() - this.startTime) * 0.001
    this.frameCount += 1

    this.lastUpdate = thisUpdate
  }

  draw () {
    this.gl.useProgram(this.currentProgram)

    // send numeric uniforms
    this.gl.uniform2fv(this.currentProgram.uniformsCache['resolution'], [this.width, this.height])
    this.gl.uniform1f(this.currentProgram.uniformsCache['time'], this.elapsedTime)
    this.gl.uniform1i(this.currentProgram.uniformsCache['frame'], this.frameCount)
    this.gl.uniform1f(this.currentProgram.uniformsCache['frameRate'], this.frameRate)
    this.gl.uniform4fv(this.currentProgram.uniformsCache['mouse'], [this.mouseClickPosition.x, this.mouseClickPosition.y, 0, 0])
    this.gl.uniform4fv(this.currentProgram.uniformsCache['date'], [this.date.getFullYear(), this.date.getMonth(), this.date.getDate(), this.date.getSeconds()])
    this.gl.uniform2fv(this.currentProgram.uniformsCache['offset'], [0, 0])

    let channelResolutionValues = [
      this.channelResolutions[0].w, this.channelResolutions[0].h, 1.0,
      this.channelResolutions[1].w, this.channelResolutions[1].h, 1.0,
      this.channelResolutions[2].w, this.channelResolutions[2].h, 1.0,
      this.channelResolutions[3].w, this.channelResolutions[3].h, 1.0
    ]

    this.gl.uniform3fv(this.currentProgram.uniformsCache['channelResolution'], channelResolutionValues)

    this.gl.uniform1i(this.currentProgram.uniformsCache['backbuffer'], 0)
    this.gl.uniform2fv(this.currentProgram.uniformsCache['surfaceSize'], [this.width, this.height])

    // send texture uniforms
    this.gl.activeTexture(this.gl.TEXTURE4)
    this.gl.bindTexture(this.gl.TEXTURE_2D, this.channels[0])
    this.gl.uniform1i(this.currentProgram.uniformsCache['channel0'], 4)

    this.gl.activeTexture(this.gl.TEXTURE5)
    this.gl.bindTexture(this.gl.TEXTURE_2D, this.channels[1])
    this.gl.uniform1i(this.currentProgram.uniformsCache['channel1'], 5)

    this.gl.activeTexture(this.gl.TEXTURE6)
    this.gl.bindTexture(this.gl.TEXTURE_2D, this.channels[2])
    this.gl.uniform1i(this.currentProgram.uniformsCache['channel2'], 6)

    this.gl.activeTexture(this.gl.TEXTURE7)
    this.gl.bindTexture(this.gl.TEXTURE_2D, this.channels[3])
    this.gl.uniform1i(this.currentProgram.uniformsCache['channel3'], 7)

    // draw
    this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.surface.buffer)
    this.gl.vertexAttribPointer(this.surface.positionAttribute, 2, this.gl.FLOAT, false, 0, 0)

    this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.buffer)
    this.gl.vertexAttribPointer(this.vertexPosition, 2, this.gl.FLOAT, false, 0, 0)

    this.gl.activeTexture(this.gl.TEXTURE0)
    this.gl.bindTexture(this.gl.TEXTURE_2D, this.backTarget.texture)

    this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, this.frontTarget.framebuffer)
    this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT)
    this.gl.drawArrays(this.gl.TRIANGLES, 0, 6)

    // screen shader

    this.gl.useProgram(this.screenProgram)
    this.gl.uniform2fv(this.screenProgram.uniformsCache['resolution'], [this.width, this.height])
    this.gl.uniform1i(this.screenProgram.uniformsCache['texture'], 1)

    this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.buffer)
    this.gl.vertexAttribPointer(this.screenVertexPosition, 2, this.gl.FLOAT, false, 0, 0)

    this.gl.activeTexture(this.gl.TEXTURE1)
    this.gl.bindTexture(this.gl.TEXTURE_2D, this.frontTarget.texture)

    // draw to screen

    this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, null)
    this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT)
    this.gl.drawArrays(this.gl.TRIANGLES, 0, 6)

    // swap buffers
    let tmp = this.frontTarget
    this.frontTarget = this.backTarget
    this.backTarget = tmp
  }

  animationFrameCallback () {
    this.update()
    this.draw()
    window.requestAnimationFrame(() => { this.animationFrameCallback() })
  }
}
