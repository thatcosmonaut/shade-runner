const {dialog} = require('electron').remote
const FileUtils = require('./file-utils.js')

function _hide (container) {
  container.classList.remove('active')
  container.classList.add('hidden')
}

function _show (container) {
  container.classList.remove('hidden')
  container.classList.add('active')
}

module.exports = class {
  constructor (containers) {
    this.shadertoyImportContainer = containers.shadertoyImportContainer
    this.shadertoyKeyInputContainer = containers.shadertoyKeyInputContainer
    this.spinnerContainer = containers.spinnerContainer
    this.shadertoyKeyInput = containers.shadertoyKeyInput
    this.shadertoyImportErrorContainer = containers.shadertoyImportErrorContainer
    this.shadertoyImportErrorMessageContainer = containers.shadertoyImportErrorMessageContainer

    this.showingShadertoyImport = false
  }

  toggleShadertoyImportContainer () {
    this.showingShadertoyImport = !this.showingShadertoyImport

    if (this.showingShadertoyImport) {
      this.shadertoyImportContainer.classList.add('active')
      this.shadertoyImportContainer.classList.remove('hidden')
    } else {
      this.shadertoyImportContainer.classList.add('hidden')
      this.shadertoyImportContainer.classList.remove('active')
    }
  }

  hideShadertoyImport () {
    _hide(this.shadertoyImportContainer)
    this.showingShadertoyImport = false
  }

  showShadertoyImport () {
    _show(this.shadertoyImportContainer)
    this.showingShadertoyImport = true
  }

  hideShadertoyKeyInput () {
    _hide(this.shadertoyKeyInputContainer)
  }

  showShadertoyKeyInput () {
    _show(this.shadertoyKeyInputContainer)
  }

  hideShadertoyImportSpinner () {
    _hide(this.spinnerContainer)
  }

  showShadertoyImportSpinner () {
    _show(this.spinnerContainer)
  }

  hideShadertoyImportError () {
    _hide(this.shadertoyImportErrorContainer)
  }

  showShadertoyImportError () {
    _show(this.shadertoyImportErrorContainer)
  }

  setShadertoyImportErrorMessage (message) {
    this.shadertoyImportErrorMessageContainer.innerHTML = message
  }

  selectShaderImportDirectory (shaderName, callback) {
    dialog.showOpenDialog({
      title: 'Import ' + shaderName + ' from Shadertoy',
      defaultPath: '~/', // is this cross-platform?
      buttonLabel: 'Select',
      properties: [
        'openDirectory',
        'createDirectory'
      ]
    }, callback)
  }
}
