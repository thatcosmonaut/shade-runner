const fs = require('fs-extra')
const chokidar = require('chokidar')

const FileUtils = {
  writeFile: function (name, content, callback) {
    fs.writeFile(name, content, callback)
  },

  makeDirIfNonexistent: function (path) {
    if (!fs.existsSync(path)) {
      fs.mkdirSync(path)
    }
  },

  // horrible kludge to write images to file
  writeImageElementToFile: function (writePath, imageElement, callback) {
    let canvas = document.createElement('canvas')
    canvas.width = imageElement.naturalWidth
    canvas.height = imageElement.naturalHeight
    canvas.getContext('2d').drawImage(imageElement, 0, 0)

    let string = canvas.toDataURL()
    let data = string.replace(/^data:image\/\w+;base64,/, '')
    let buffer = new window.Buffer(data, 'base64')

    fs.writeFile(writePath, buffer, callback)
  },

  setFileWatch: function (path, readCallback, changeCallback) {
    document.title = 'Shade Runner - ' + path

    let watcher = chokidar.watch(path)
    watcher.on('change', () => {
      fs.readFile(path, 'utf8', changeCallback)
    })

    fs.readFile(path, 'utf8', readCallback)
  }
}

// what if URL is bad
FileUtils.saveImageFromURL = function (url, savePath, callback) {
  let image = new Image()
  image.onload = () => {
    FileUtils.writeImageElementToFile(savePath, image, (err) => {
      callback(err)
    })
  }
  image.src = url
}

module.exports = FileUtils
