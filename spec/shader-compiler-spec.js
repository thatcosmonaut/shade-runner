const ShaderCompiler = require('../js/shader-compiler.js')
const DefaultGenerator = require('../js/generators/default.js')

const ErrorDisplayer = class {
  constructor (container) {
    this.error = ''
  }

  displayError (error) {
    this.error = error
  }
}

const invalidVertexCode = `#version 300 es
  precision mediump float;

  in vec2e position;
  void main()
  {
    gl_Position = vec4(position, 0.0, 1.0);
  }
`

const invalidFragmentCode = `#version 300 es
  precision mediump float;

  uniform vec2 resolution;
  uniform float; time;

  out vec4 fragColor;
  void main()
  {
    vec2 uv = gl_FragCoord.xy / resolution.xy;
    fragColor = vec4(uv, 0.5 + 0.5*sin(time), 1.0);
  }
`

const chai = require('chai')
let expect = chai.expect

describe('ShaderCompiler', function () {
  let canvas = document.createElement('canvas')
  let gl = canvas.getContext('webgl2')
  let errorDisplayer = new ErrorDisplayer()
  let shaderCompiler = new ShaderCompiler(gl, errorDisplayer)

  describe('compileVertexShader', function () {
    describe('if code is valid', function () {
      let source = DefaultGenerator.vertexShader

      it('should return the compiled vertex shader', function () {
        expect(shaderCompiler.compileVertexShader(source)).to.be.a('webglshader')
      })
    })

    describe('if code is invalid', function () {
      let source = invalidVertexCode

      it('should return null', function () {
        expect(shaderCompiler.compileVertexShader(source)).to.be.null
      })

      it('should display error', function () {
        shaderCompiler.compileVertexShader(source)
        expect(errorDisplayer.error).to.contain('ERROR')
        expect(errorDisplayer.error).to.contain('0:4')
      })
    })
  })

  describe('compileFragmentShader', function () {
    describe('if code is valid', function () {
      let source = DefaultGenerator.fragmentShader

      it('should return the compiled fragment shader', function () {
        expect(shaderCompiler.compileFragmentShader(source)).to.be.a('webglshader')
      })
    })

    describe('if code is invalid', function () {
      let source = invalidFragmentCode

      it('should return null', function () {
        expect(shaderCompiler.compileFragmentShader(source)).to.be.null
      })

      it('should display error', function () {
        shaderCompiler.compileFragmentShader(source)
        expect(errorDisplayer.error).to.contain('ERROR')
        expect(errorDisplayer.error).to.contain('0:5')
      })
    })
  })

  describe('link', function () {
    describe('if program is valid', function () {
      let vertexShader = shaderCompiler.compileVertexShader(DefaultGenerator.vertexShader)
      let fragmentShader = shaderCompiler.compileFragmentShader(DefaultGenerator.fragmentShader)

      it('should return a web gl shader program', function () {
        expect(shaderCompiler.link(vertexShader, fragmentShader)).to.be.a('webglprogram')
      })
    })

    describe('if shader versions do not match', function () {
      // in this example, the shaders have mismatched versions
      let vertexCode = `precision mediump float;

        varying vec2 position;
        void main()
        {
          gl_Position = vec4(position, 0.0, 1.0);
        }
      `

      let vertexShader = shaderCompiler.compileVertexShader(vertexCode)
      let fragmentShader = shaderCompiler.compileFragmentShader(DefaultGenerator.fragmentShader)

      it('should return null', function () {
        expect(shaderCompiler.link(vertexShader, fragmentShader)).to.be.null
      })

      it('should display error', function () {
        shaderCompiler.link(vertexShader, fragmentShader)
        expect(errorDisplayer.error).to.contain('Versions of linked shaders have to match')
      })
    })
  })
})
