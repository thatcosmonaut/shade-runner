const UI = require('../js/ui.js')

const chai = require('chai')
const expect = chai.expect

describe('UI', function () {
  let shadertoyImportContainer = document.createElement('div')
  let shadertoyKeyInputContainer = document.createElement('div')
  let spinnerContainer = document.createElement('div')
  let shadertoyKeyInput = document.createElement('div')
  let shadertoyImportErrorContainer = document.createElement('div')
  let shadertoyImportErrorMessageContainer = document.createElement('div')

  let ui = new UI({
    shadertoyImportContainer: shadertoyImportContainer,
    shadertoyKeyInputContainer: shadertoyKeyInputContainer,
    spinnerContainer: spinnerContainer,
    shadertoyKeyInput: shadertoyKeyInput,
    shadertoyImportErrorContainer: shadertoyImportErrorContainer,
    shadertoyImportErrorMessageContainer: shadertoyImportErrorMessageContainer
  })

  let hideTest = function (container, method) {
    beforeEach(function () {
      container.classList.remove('hidden')
      container.classList.add('active')
      method()
    })

    it('should not be active', function () {
      expect(container.classList.contains('active')).to.be.false
    })

    it('should be hidden', function () {
      expect(container.classList.contains('hidden')).to.be.true
    })
  }

  let showTest = function (container, method) {
    beforeEach(function () {
      container.classList.remove('active')
      container.classList.add('hidden')
      method()
    })

    it('should be active', function () {
      expect(container.classList.contains('active')).to.be.true
    })

    it('should not be hidden', function () {
      expect(container.classList.contains('hidden')).to.be.false
    })
  }

  describe('hideShadertoyKeyInput', function () {
    hideTest(ui.shadertoyKeyInputContainer, ui.hideShadertoyKeyInput.bind(ui))
  })

  describe('showShadertoyKeyInput', function () {
    showTest(ui.shadertoyKeyInputContainer, ui.showShadertoyKeyInput.bind(ui))
  })

  describe('hideShadertoyImportSpinner', function () {
    hideTest(ui.spinnerContainer, ui.hideShadertoyImportSpinner.bind(ui))
  })

  describe('showShadertoyImportSpinner', function () {
    showTest(ui.spinnerContainer, ui.showShadertoyImportSpinner.bind(ui))
  })

  describe('hideShadertoyImportError', function () {
    hideTest(ui.shadertoyImportErrorContainer, ui.hideShadertoyImportError.bind(ui))
  })

  describe('showShadertoyImportError', function () {
    showTest(ui.shadertoyImportErrorContainer, ui.showShadertoyImportError.bind(ui))
  })

  describe('hideShadertoyImport', function () {
    hideTest(ui.shadertoyImportContainer, ui.hideShadertoyImport.bind(ui))

    it('should set showing value to false', function () {
      expect(ui.showingShadertoyImport).to.be.false
    })
  })

  describe('showShadertoyImport', function () {
    showTest(ui.shadertoyImportContainer, ui.showShadertoyImport.bind(ui))

    it('should set showing value to true', function () {
      expect(ui.showingShadertoyImport).to.be.true
    })
  })

  describe('toggleShadertoyImportContainer', function () {
    describe('if showing container', function () {
      beforeEach(function () {
        ui.showingShadertoyImport = true
        shadertoyImportContainer.classList.remove('hidden')
        shadertoyImportContainer.classList.add('active')
        ui.toggleShadertoyImportContainer()
      })

      it('should not be active', function () {
        expect(ui.shadertoyImportContainer.classList.contains('active')).to.be.false
      })

      it('should be hidden', function () {
        expect(ui.shadertoyImportContainer.classList.contains('hidden')).to.be.true
      })
    })

    describe('if not showing container', function () {
      beforeEach(function () {
        ui.showingShadertoyImport = false
        shadertoyImportContainer.classList.remove('active')
        shadertoyImportContainer.classList.add('hidden')
        ui.toggleShadertoyImportContainer()
      })

      it('should be active', function () {
        expect(ui.shadertoyImportContainer.classList.contains('active')).to.be.true
      })

      it('should not be hidden', function () {
        expect(ui.shadertoyImportContainer.classList.contains('hidden')).to.be.false
      })
    })
  })

  describe('setShadertoyImportErrorMessage', function () {
    before(function () {
      ui.setShadertoyImportErrorMessage('hello')
    })

    it('should display error message', function () {
      expect(ui.shadertoyImportErrorMessageContainer.innerHTML).to.equal('hello')
    })
  })
})
