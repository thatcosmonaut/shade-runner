const TextureUtils = require('../js/texture-utils.js')

const chai = require('chai')
const expect = chai.expect

describe('TextureUtils', function () {
  describe('loadTextureFromURL', function () {
    let canvas = document.createElement('canvas')
    let gl = canvas.getContext('webgl2')

    it('should call back with texture and size', function (done) {
      TextureUtils.loadTextureFromURL(gl, process.cwd() + '/images/blank.png', function (texture, width, height) {
        expect(texture).to.be.a('webgltexture')
        expect(width).to.equal(1)
        expect(height).to.equal(1)
        done()
      })
    })
  })
})
